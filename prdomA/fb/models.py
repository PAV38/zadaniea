# -*- coding: utf-8 -*-
from cms.models import CMSPlugin
from django.db import models
from django.utils.translation import ugettext_lazy as _

class FBModel(CMSPlugin):
    LAYOUTS = [
        ('standard', ('standard')),
        ('box_count', ('box_count')),
        ('button_count', ('button_count')),
    ]
    STYLES = [
        ('light', ('light')),
        ('dark', ('dark')),
    ]
    layout = models.CharField(("layout"), choices=LAYOUTS, default='standard', max_length=50)
    style = models.CharField(("style"), choices=STYLES, default='light', max_length=50)
    width = models.PositiveSmallIntegerField(("width"), max_length=4, default=450)