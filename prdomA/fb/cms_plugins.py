# -*- coding: utf-8 -*-
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from fb import models

class FacebookLikePlugin(CMSPluginBase):
    model = models.FBModel
    name = 'Facebook'
    render_template = 'fb.html'

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

plugin_pool.register_plugin(FacebookLikePlugin)