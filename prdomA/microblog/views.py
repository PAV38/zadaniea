#!/usr/bin/python
# -*- coding: utf-8 -*-
import random
import string
from django.shortcuts import render, get_list_or_404, redirect, render_to_response
from django.contrib import messages
from django.http import Http404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
from django.http import HttpResponseRedirect
from microblog.forms import LoginForm, EntryForm, RegistrationForm
from django.forms.util import ErrorList
from django.contrib import messages
from django.core.mail import send_mail
from microblog.models import Entry, Tag, UserProfile
from django.template import RequestContext

def wszystkie_posty(request, message=None):
    wpisy = Entry.objects.order_by('-pub_date')
    title = "wszystkie posty"
    return render(request, 'microblog/wszystkie_posty.html', {'wpisy': wpisy, 'title': title, 'message': message})

def posty_uzytkownika(request, id):
    uzytkownik = User.objects.get(pk=id)
    if uzytkownik:
        title = "posty uzytkownika '%s'" % uzytkownik.username
        wpisy = Entry.objects.all().filter(user_id=id).order_by('-pub_date')
        return render_to_response('microblog/wszystkie_posty.html', locals(), context_instance=RequestContext(request))
    raise Http404

def posty_tag(request, id):
    tag = Tag.objects.get(pk=id)
    if tag:
        title = "posty tagu '%s'" % tag.text
        wpisy = Entry.objects.all().filter(tags__pk=id).order_by('-pub_date')
        return render_to_response('microblog/wszystkie_posty.html', locals(), context_instance=RequestContext(request))
    raise Http404

def nowy_wpis(request):
    if not request.user.is_authenticated():
        raise Http404()
    if request.method == "POST":
        form = EntryForm(request.POST)
        if form.is_valid():
            post = Entry(
                title=form.cleaned_data['title'],
                text=form.cleaned_data['text'],
                pub_date = timezone.now(),
                user = request.user
            )
            post.save()
            for tag in form.cleaned_data['tags']:
                post.tags.add(tag)
            post.save()
            return redirect('wszystkie_posty')
    else:
        form = EntryForm()
    return render_to_response('microblog/nowy_post.html', locals(), context_instance=RequestContext(request))

def aktywacja(request, key):
    if request.user.is_authenticated():
        raise Http404()
    profil = UserProfile.objects.get(key=key)
    if profil:
        uzytkownik = User.objects.get(pk=profil.user.id)
        uzytkownik.is_active = True
        uzytkownik.save()
        profil.delete()
        messages.info(request, 'Aktywowano. Możesz się logować.')
        return redirect('wszystkie_posty')
    raise Http404()

def edycja(request, id):
    entry = Entry.objects.get(pk=id)
    editable = request.user.groups.filter(name='moderator') or (entry.user.id == request.user.id and entry.can_edit)
    if entry and request.user.is_authenticated and editable:
        if request.method == "POST":
            form = EntryForm(request.POST)
            if form.is_valid():
                entry.title = form.cleaned_data['title']
                entry.text = form.cleaned_data['text']
                entry.tags.clear()
                for tag in form.cleaned_data['tags']:
                    entry.tags.add(tag)
                entry.mod_date = timezone.now()
                entry.user2 = request.user
                entry.save()
                messages.info(request, 'Zapisano.')
                return redirect('wszystkie_posty')
        else:
            form = EntryForm(initial={'title': entry.title, 'tags': entry.tags.all(), 'text': entry.text})
        return render_to_response('microblog/edycja.html', locals(), context_instance=RequestContext(request))
    raise Http404()

def rejestracja(request):
    if request.user.is_authenticated():
        raise Http404()

    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            uzytkownik = User.objects.create(
                username=form.cleaned_data['login'],
                email=form.cleaned_data['email'],
                is_active=False
            )
            uzytkownik.set_password(form.cleaned_data['password1'])
            uzytkownik.save()
            profile = UserProfile(user=uzytkownik, key=''.join(random.sample(string.lowercase+string.digits,10)))
            profile.save()
            # odkomentować potem
            #link = "http://http://194.29.175.240/~p5/microblog.wsgi/blog/uzytkownicy/aktywuj/%s " % profile.key
            #send_mail('Link aktywacyjny', link, 'blog@blog.pl', [form.cleaned_data['email']])
            messages.info(request, 'Zarejestrowano. Odbierz maila i kliknij link aktywacyjny.')
            return redirect('wszystkie_posty')
    else:
        form = RegistrationForm()
    return render_to_response('microblog/rejestracja.html', locals(), context_instance=RequestContext(request))

def month(request,month_id):
    title = "Posty z miesiaca: " + month_id
    wpisy = Entry.objects.raw("SELECT * FROM microblog_entry WHERE strftime('%%m', pub_date)='" + month_id + "'")
    logged = True
    return render_to_response('microblog/wszystkie_posty.html', locals(), context_instance=RequestContext(request))

def logowanie(request):
    if request.user.is_authenticated():
        raise Http404()
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            uzytkownik = authenticate(
                username=form.cleaned_data['login'],
                password=form.cleaned_data['password']
            )
            if uzytkownik:
                if uzytkownik.is_active:
                    login(request, uzytkownik)
                    messages.info(request, 'Zalogowano')
                else:
                    messages.error(request, 'Użytkownik nieaktywny. Kliknij link z maila.')
                return redirect('wszystkie_posty')
            else:
                 messages.info(request, 'Dane nieprawidłowe')
    else:
        form = LoginForm()
    return render_to_response('microblog/logowanie.html', locals(), context_instance=RequestContext(request))

def wylogowanie(request):
    if request.user.is_authenticated():
        logout(request)
        messages.info(request, 'wylogowano')
    else:
        raise Http404()
    return redirect('wszystkie_posty')
