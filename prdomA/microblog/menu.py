# -*- coding: utf-8 -*-
from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from models import Entry
import datetime

class BlogMenu(CMSAttachMenu):
    name = _("Blog menu")

    def get_nodes(self, request):
        menu = []

        if request.user.is_authenticated():
            nowy_wpis = NavigationNode(u'Nowy wpis', reverse('nowy_wpis'), 1)
            menu.append(nowy_wpis)
            wylogowanie = NavigationNode(u'Wyloguj się', reverse('wylogowanie'), 2)
            menu.append(wylogowanie)
        else:
            logowanie = NavigationNode(u'Zaloguj się', reverse('logowanie'), 3)
            menu.append(logowanie)
            rejestracja = NavigationNode(u'Zarejestruj się', reverse('rejestracja'), 4)
            menu.append(rejestracja)

        uzytkownicy = NavigationNode(u'Uzytkownicy', reverse('wszystkie_posty'), 5)
        menu.append(uzytkownicy)

        users = User.objects.all()

        for user in users:
            us = NavigationNode(user.username, reverse('posty_uzytkownika', args=[user.pk]), user.pk + 500, 5)
            menu.append(us)

        months = NavigationNode(u'Miesiące', reverse('wszystkie_posty'), 6)
        menu.append(months)

        months_ = Entry.objects.raw("SELECT id, strftime('%%m', pub_date) as month, pub_date FROM microblog_entry GROUP BY strftime('%%m', pub_date)")
        for item in months_:
            url = reverse('miesiac', args=[item.month])
            menu.append(NavigationNode(datetime.datetime.strftime(item.pub_date, '%m'), url, item.id + 500, 6))

        return menu
menu_pool.register_menu(BlogMenu)
