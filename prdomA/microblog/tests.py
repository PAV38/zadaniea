#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.utils import unittest
from django.db import connection
from django.contrib.auth.models import User
from microblog.models import Entry
from django.test import Client
import re

class SampleDatabaseTestCase(unittest.TestCase):
    # test połączenia z bazą
    def test_database(self):
        cursor = connection.cursor()
        cursor.execute('PRAGMA table_info(microblog_entry);')
        self.assertNotEqual(cursor.fetchall(), None)
        wpisy = Entry.objects.all()
        self.assertEqual(len(wpisy) == 0, True)

class MicroblogUserPostsTestCase(unittest.TestCase):
    # test autentykacji i wiadomości błędów
    def test_user_messages(self):
        user = User.objects.create(username='mpiasecki')
        user.set_password('mpiasecki')
        user.save()
        client = Client()
        response = client.post("/blog/zaloguj", {'username': 'złylogin', 'password': 'złe hasło'})
        self.assertEqual(re.search('Błędny login lub hasło!', response.content).group(0), 'Błędny login lub hasło!')
