from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$', views.wszystkie_posty, name="wszystkie_posty"),
    url(r'^uzytkownicy/(?P<id>\d+)$', views.posty_uzytkownika, name="posty_uzytkownika"), #
    url(r'^edycja/(?P<id>\d+)$', views.edycja, name="edycja_post"), #
    url(r'^tagi/(?P<id>\d+)$', views.posty_tag, name="posty_tag"), #
    url(r'^uzytkownicy/aktywacja/(?P<key>.+)$', views.aktywacja, name="aktywacja"), #
    url(r'^zaloguj$', views.logowanie, name="logowanie"), #
    url(r'^wyloguj$', views.wylogowanie, name="wylogowanie"), #
    url(r'^wpisy/nowy', views.nowy_wpis, name="nowy_wpis"),
    url(r'^nowy_user$', views.rejestracja, name="rejestracja"), #
    url(r'^miesiac/(?P<month_id>.+)$', views.month, name="miesiac") #
)
