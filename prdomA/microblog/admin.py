from django.contrib import admin
from microblog.models import Tag, Entry, UserProfile
from django.contrib.auth.models import User

admin.site.register(Tag)
admin.site.register(UserProfile)

class EntryAdmin(admin.ModelAdmin):
    list_filter = ('pub_date', )
    ordering = ('pub_date', )
    search_fields = ('tags__text',)


admin.site.register(Entry, EntryAdmin)


