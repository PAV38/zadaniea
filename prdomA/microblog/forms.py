#!/usr/bin/python
# -*- coding: utf-8 -*-

from django import forms
from microblog.models import Entry, Tag
from django.forms import ModelForm

from django.contrib.auth.models import User

attrs_dict = { 'class': 'required' }


class LoginForm(forms.Form):
    login = forms.CharField(max_length=40)
    password = forms.CharField(max_length=40, widget=forms.PasswordInput())

class EntryForm(forms.Form):
    text = forms.CharField(max_length=200, widget=forms.Textarea())
    title = forms.CharField(max_length=200)
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(), required=False, widget=forms.CheckboxSelectMultiple)

class RegistrationForm(forms.Form):
    login = forms.CharField(max_length=30,
                               widget=forms.TextInput(attrs=attrs_dict))
    email = forms.EmailField(widget=forms.TextInput(attrs=dict(attrs_dict,
                                                               maxlength=75)))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs=attrs_dict, render_value=False))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs=attrs_dict, render_value=False))

    def clean_login(self):
        try:
            user = User.objects.get(username=self.cleaned_data['login'])
        except User.DoesNotExist:
            return self.cleaned_data['login']
        raise forms.ValidationError((u'Nazwa użytkownika zajęta.'))

    def clean(self):
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError((u'Musisz podać 2 razy to samo hasło.'))
        return self.cleaned_data