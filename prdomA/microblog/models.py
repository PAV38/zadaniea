from django.contrib.auth.models import User
from django.db import models
from django import forms
from django.utils import timezone


class Tag(models.Model):
    text = models.CharField(max_length=200)

    def __unicode__(self):
        return self.text

class Entry(models.Model):
    text = models.TextField(max_length=200)
    title = models.CharField(max_length=200)
    pub_date = models.DateTimeField()
    mod_date = models.DateTimeField(null=True, blank=True)
    user = models.ForeignKey(User)
    user2 = models.ForeignKey(User, related_name='modification', null=True, blank=True)
    tags = models.ManyToManyField(Tag, null=True, blank=True)

    def can_edit(self):
        if self.modified:
            return (timezone.now() - self.modified).seconds < 600
        else:
            return (timezone.now() - self.created).seconds < 600

    def __unicode__(self):
        return self.title

class UserProfile(models.Model):
    user = models.ForeignKey(User)
    key = models.CharField(max_length=10)