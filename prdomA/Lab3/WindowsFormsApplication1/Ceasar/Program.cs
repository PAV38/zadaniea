﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Ceasar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello.\nPlease intert your message:\n");
            string message = Console.ReadLine();
            
            Console.Clear();
            Console.Write("Please insert your key (integer values): ");
            int key = Convert.ToInt32(Console.ReadLine());

            Console.Clear();
            string output = Ceasar.CeasarEncryption(message, key);
            Console.WriteLine("Your encrypted message with key value {0} is:", key);
            Console.WriteLine(output);
            Console.WriteLine("\nYour message was:");
            Console.WriteLine(Ceasar.CeasarDecryption(output, key));

            //Saving
            Console.WriteLine("\n\nEnter name for input file:");
            string inputFileName = Console.ReadLine();
            int indexIn = 0;
            while (File.Exists(inputFileName))
            {
                inputFileName += indexIn;
            }
            Console.WriteLine("\nEnter name for output file:");
            string outputFileName = Console.ReadLine();
            int indexOut = 0;
            while (File.Exists(outputFileName))
            {
                outputFileName += indexOut;
            }

            if (inputFileName != "")
            {
                inputFileName += ".txt";
                using (StreamWriter sr = new StreamWriter(inputFileName))
                {
                    sr.WriteLine(message);
                    sr.WriteLine(key);
                }
            }
            if (outputFileName != "")
            {
                outputFileName += ".txt";
                using (StreamWriter sr = new StreamWriter(outputFileName))
                {
                    sr.WriteLine(output);
                    sr.WriteLine(key);
                }
            }
            Console.WriteLine("\n\nYour input and output has been saved to files:\n{0}\n{1}",inputFileName, outputFileName);

            Console.ReadKey();
        }
    }
}
