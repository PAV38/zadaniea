﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ceasar
{
    public static class Ceasar
    {
        public static string CeasarEncryption(string message, int key)
        {
            char[] buffer = message.ToCharArray();
            for (int i = 0; i < buffer.Length; i++)
            {
                char letter = buffer[i];

                if (letter >= 'a' && letter <= 'z')
                {
                    letter = (char)(letter + key);

                    if (letter > 'z')
                    {
                        letter = (char)(letter - 26);
                    }
                    else if (letter < 'a')
                    {
                        letter = (char)(letter + 26);
                    }
                }

                if (letter >= 'A' && letter <= 'Z')
                {
                    letter = (char)(letter + key);

                    if (letter > 'Z')
                    {
                        letter = (char)(letter - 26);
                    }
                    else if (letter < 'A')
                    {
                        letter = (char)(letter + 26);
                    }
                }

                buffer[i] = letter;
            }

            return new string(buffer);
        }

        public static string CeasarDecryption(string ciphertext, int key)
        {
            char[] buffer = ciphertext.ToCharArray();
            for (int i = 0; i < buffer.Length; i++)
            {
                char letter = buffer[i];

                if (letter >= 'a' && letter <= 'z')
                {
                    letter = (char)(letter - key);

                    if (letter > 'z')
                    {
                        letter = (char)(letter - 26);
                    }
                    else if (letter < 'a')
                    {
                        letter = (char)(letter + 26);
                    }
                }

                if (letter >= 'A' && letter <= 'Z')
                {
                    letter = (char)(letter - key);

                    if (letter > 'Z')
                    {
                        letter = (char)(letter - 26);
                    }
                    else if (letter < 'A')
                    {
                        letter = (char)(letter + 26);
                    }
                }

                buffer[i] = letter;
            }

            return new string(buffer);
        }
    }
}
