﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuPasek = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wczytajTekstJawnyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wczytajTekstZaszyfrowanyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.akcjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.znajdźKluczToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.szyfrujToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.szyfrCezaraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deszyfrujToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deszyfrujCezaraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jawnyLabel = new System.Windows.Forms.Label();
            this.szyfrLabel = new System.Windows.Forms.Label();
            this.jawnyTB = new System.Windows.Forms.TextBox();
            this.szyfrTB = new System.Windows.Forms.TextBox();
            this.kluczLabel = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuPasek.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuPasek
            // 
            this.menuPasek.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem,
            this.akcjeToolStripMenuItem,
            this.szyfrujToolStripMenuItem,
            this.deszyfrujToolStripMenuItem});
            this.menuPasek.Location = new System.Drawing.Point(0, 0);
            this.menuPasek.Name = "menuPasek";
            this.menuPasek.Size = new System.Drawing.Size(784, 24);
            this.menuPasek.TabIndex = 1;
            this.menuPasek.Text = "menuStrip2";
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wczytajTekstJawnyToolStripMenuItem,
            this.wczytajTekstZaszyfrowanyToolStripMenuItem});
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.plikToolStripMenuItem.Text = "Plik";
            // 
            // wczytajTekstJawnyToolStripMenuItem
            // 
            this.wczytajTekstJawnyToolStripMenuItem.Name = "wczytajTekstJawnyToolStripMenuItem";
            this.wczytajTekstJawnyToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.wczytajTekstJawnyToolStripMenuItem.Text = "Wczytaj tekst jawny";
            this.wczytajTekstJawnyToolStripMenuItem.Click += new System.EventHandler(this.wczytajTekstJawnyToolStripMenuItem_Click);
            // 
            // wczytajTekstZaszyfrowanyToolStripMenuItem
            // 
            this.wczytajTekstZaszyfrowanyToolStripMenuItem.Name = "wczytajTekstZaszyfrowanyToolStripMenuItem";
            this.wczytajTekstZaszyfrowanyToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.wczytajTekstZaszyfrowanyToolStripMenuItem.Text = "Wczytaj tekst zaszyfrowany";
            this.wczytajTekstZaszyfrowanyToolStripMenuItem.Click += new System.EventHandler(this.wczytajTekstZaszyfrowanyToolStripMenuItem_Click);
            // 
            // akcjeToolStripMenuItem
            // 
            this.akcjeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.znajdźKluczToolStripMenuItem});
            this.akcjeToolStripMenuItem.Name = "akcjeToolStripMenuItem";
            this.akcjeToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.akcjeToolStripMenuItem.Text = "Akcje";
            // 
            // znajdźKluczToolStripMenuItem
            // 
            this.znajdźKluczToolStripMenuItem.Name = "znajdźKluczToolStripMenuItem";
            this.znajdźKluczToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.znajdźKluczToolStripMenuItem.Text = "Znajdź klucz";
            this.znajdźKluczToolStripMenuItem.Click += new System.EventHandler(this.znajdźKluczToolStripMenuItem_Click);
            // 
            // szyfrujToolStripMenuItem
            // 
            this.szyfrujToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.szyfrCezaraToolStripMenuItem});
            this.szyfrujToolStripMenuItem.Name = "szyfrujToolStripMenuItem";
            this.szyfrujToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.szyfrujToolStripMenuItem.Text = "Szyfruj";
            // 
            // szyfrCezaraToolStripMenuItem
            // 
            this.szyfrCezaraToolStripMenuItem.Name = "szyfrCezaraToolStripMenuItem";
            this.szyfrCezaraToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.szyfrCezaraToolStripMenuItem.Text = "Szyfr Cezara";
            this.szyfrCezaraToolStripMenuItem.Click += new System.EventHandler(this.szyfrCezaraToolStripMenuItem_Click);
            // 
            // deszyfrujToolStripMenuItem
            // 
            this.deszyfrujToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deszyfrujCezaraToolStripMenuItem});
            this.deszyfrujToolStripMenuItem.Name = "deszyfrujToolStripMenuItem";
            this.deszyfrujToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.deszyfrujToolStripMenuItem.Text = "Deszyfruj";
            // 
            // deszyfrujCezaraToolStripMenuItem
            // 
            this.deszyfrujCezaraToolStripMenuItem.Name = "deszyfrujCezaraToolStripMenuItem";
            this.deszyfrujCezaraToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.deszyfrujCezaraToolStripMenuItem.Text = "Deszyfruj Cezara";
            this.deszyfrujCezaraToolStripMenuItem.Click += new System.EventHandler(this.deszyfrujCezaraToolStripMenuItem_Click);
            // 
            // jawnyLabel
            // 
            this.jawnyLabel.AutoSize = true;
            this.jawnyLabel.Location = new System.Drawing.Point(12, 35);
            this.jawnyLabel.Name = "jawnyLabel";
            this.jawnyLabel.Size = new System.Drawing.Size(67, 13);
            this.jawnyLabel.TabIndex = 2;
            this.jawnyLabel.Text = "Tekst jawny:";
            // 
            // szyfrLabel
            // 
            this.szyfrLabel.AutoSize = true;
            this.szyfrLabel.Location = new System.Drawing.Point(12, 311);
            this.szyfrLabel.Name = "szyfrLabel";
            this.szyfrLabel.Size = new System.Drawing.Size(103, 13);
            this.szyfrLabel.TabIndex = 3;
            this.szyfrLabel.Text = "Tekst zaszyfrowany:";
            // 
            // jawnyTB
            // 
            this.jawnyTB.Location = new System.Drawing.Point(15, 51);
            this.jawnyTB.Multiline = true;
            this.jawnyTB.Name = "jawnyTB";
            this.jawnyTB.Size = new System.Drawing.Size(757, 223);
            this.jawnyTB.TabIndex = 4;
            // 
            // szyfrTB
            // 
            this.szyfrTB.Location = new System.Drawing.Point(15, 327);
            this.szyfrTB.Multiline = true;
            this.szyfrTB.Name = "szyfrTB";
            this.szyfrTB.Size = new System.Drawing.Size(757, 223);
            this.szyfrTB.TabIndex = 5;
            // 
            // kluczLabel
            // 
            this.kluczLabel.AutoSize = true;
            this.kluczLabel.Location = new System.Drawing.Point(224, 24);
            this.kluczLabel.Name = "kluczLabel";
            this.kluczLabel.Size = new System.Drawing.Size(160, 13);
            this.kluczLabel.TabIndex = 6;
            this.kluczLabel.Text = "Twój szukany klucz ma wartość:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.kluczLabel);
            this.Controls.Add(this.szyfrTB);
            this.Controls.Add(this.jawnyTB);
            this.Controls.Add(this.szyfrLabel);
            this.Controls.Add(this.jawnyLabel);
            this.Controls.Add(this.menuPasek);
            this.Name = "Form1";
            this.Text = "Caesar Killer";
            this.menuPasek.ResumeLayout(false);
            this.menuPasek.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuPasek;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wczytajTekstJawnyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wczytajTekstZaszyfrowanyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem akcjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem znajdźKluczToolStripMenuItem;
        private System.Windows.Forms.Label jawnyLabel;
        private System.Windows.Forms.Label szyfrLabel;
        private System.Windows.Forms.TextBox jawnyTB;
        private System.Windows.Forms.TextBox szyfrTB;
        private System.Windows.Forms.Label kluczLabel;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem szyfrujToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem szyfrCezaraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deszyfrujToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deszyfrujCezaraToolStripMenuItem;

    }
}

