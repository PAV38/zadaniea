﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Ceasar;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private string _nazwaPliku;
        private string _jawnyTekst;
        private string _zaszyfrowanyTekst;
        private int _klucz;
        private bool _kluczZnaleziony;

        public Form1()
        {
            InitializeComponent();
        }

        private void wczytajTekstJawnyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                jawnyTB.Text = "";
                _nazwaPliku = openFileDialog1.FileName;
                using (StreamReader sr = new StreamReader(File.Open(_nazwaPliku, FileMode.Open), Encoding.UTF8))
                {
                    try
                    {
                        _jawnyTekst = sr.ReadToEnd();
                    }
                    catch (IOException ioex)
                    {
                        MessageBox.Show("ERROR\n" + ioex.Message);
                    }
                }
                MessageBox.Show("Wczytano tekst jawny!");

                jawnyTB.Text = _jawnyTekst;
            }
        }

        private void wczytajTekstZaszyfrowanyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                szyfrTB.Text = "";
                _nazwaPliku = openFileDialog1.FileName;
                using (StreamReader sr = new StreamReader(File.Open(_nazwaPliku, FileMode.Open), Encoding.UTF8))
                {
                    try
                    {
                        _zaszyfrowanyTekst = sr.ReadToEnd();
                    }
                    catch (IOException ioex)
                    {
                        MessageBox.Show("ERROR\n" + ioex.Message);
                    }
                }
                MessageBox.Show("Wczytano zaszyfrowany tekst!");

                szyfrTB.Text = _zaszyfrowanyTekst;
            }
        }

        private void znajdźKluczToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _jawnyTekst = jawnyTB.Text;
            _zaszyfrowanyTekst = szyfrTB.Text;

            if (_jawnyTekst.Length != _zaszyfrowanyTekst.Length)
            {
                kluczLabel.Text = "Twój szukany klucz ma wartość: ";

                MessageBox.Show("Podane teksty są różnej długości zatem nie można dla nich znaleźć klucza!");
            }
            else
            {
                _klucz = 0;
                _kluczZnaleziony = false;
                char jawnaLitera = _jawnyTekst[0];
                char niejawnaLitera = _zaszyfrowanyTekst[0];

                while (!_kluczZnaleziony)
                {
                    if (jawnaLitera == niejawnaLitera)
                    {
                        _kluczZnaleziony = true;
                    }
                    else
                    {
                        if (jawnaLitera == 'z')
                        {
                            jawnaLitera = 'a';
                        }
                        else if (jawnaLitera == 'Z')
                        {
                            jawnaLitera = 'A';
                        }
                        else
                        {
                            jawnaLitera++;
                        }
                        _klucz++;
                    }
                }

                MessageBox.Show("Znaleziono klucz!");

                kluczLabel.Text = "Twój szukany klucz ma wartość: " + _klucz;
            }

            //if (_zaszyfrowanyTekst[0] > _jawnyTekst[0])
            //{
            //    _klucz = _zaszyfrowanyTekst[0] - _jawnyTekst[0];
            //}
            //else
            //{
            //    _klucz = _jawnyTekst[0] - _zaszyfrowanyTekst[0];
            //}
        }

        private void szyfrCezaraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int key = 2;
            szyfrTB.Text = "";
            if (jawnyTB.Text == "")
            {
                jawnyTB.Text = "Wpisz tu wiadomość, aby zaszyfrować";
            }
            else
            {
                string plainText = jawnyTB.Text;
                string cipherText = Ceasar.Ceasar.CeasarEncryption(plainText, key);
                szyfrTB.Text = cipherText;
                kluczLabel.Text = "Klucz szyfru cezara: " + key;
            }
        }

        private void deszyfrujCezaraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int key = 2;
            jawnyTB.Text = "";
            if (szyfrTB.Text == "")
            {
                szyfrTB.Text = "Wpisz tu wiadomość, abyt odszyfrować";
            }
            else
            {
                string cipherText = szyfrTB.Text;
                string plainText = Ceasar.Ceasar.CeasarDecryption(cipherText, key);
                jawnyTB.Text = plainText;
                kluczLabel.Text = "Klucz szyfru cezara: " + key;
            }
        }
    }
}
