# -*- coding: utf-8 -*-
from django import forms

# FORMULARZ KONTAKTOWY
class ContactForm(forms.Form):
    email = forms.EmailField(max_length=50, min_length=5)
    subject = forms.CharField(max_length=50, min_length=5)
    content = forms.CharField(widget=forms.Textarea(), min_length=10)
