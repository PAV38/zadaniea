from django import forms

class CommentsForm(forms.Form):
    user = forms.CharField(max_length=100, min_length=3)
    content = forms.CharField(max_length=1000, min_length=5)
