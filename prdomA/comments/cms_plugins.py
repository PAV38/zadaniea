from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from comments.models import Comment, Comments
from comments.forms import CommentsForm

class CommentsPlugin(CMSPluginBase):
        model = Comments
        name = _("Komentarze")
        render_template = "comments.html"

        def render(self, context, instance, placeholder):
                request = context['request']
                if request.method == "POST":
                        form = CommentsForm(request.POST)
                        if form.is_valid():
                                comment = Comment(user=form.cleaned_data['user'], content=form.cleaned_data['content'])
                                comment.save()
                                instance.comments.add(comment)
                context.update({'instance': instance, 'form': CommentsForm(), 'comments': instance.comments.all().order_by('-date'),})

                return context

plugin_pool.register_plugin(CommentsPlugin)